import random

from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from .models import Page

html_template_bootstrap = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
     <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  </head>
  <body>
    {body}
    
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  </body>
</html>
"""

html_template_css = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <link href="main.css" rel="stylesheet">
    <meta charset="utf-8" />
    <title>Django CMS</title> 
  </head>
  <body>
    {body}
  </body>
</html>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"

def index(request):

    pages = Page.objects.all()
    if len(pages) == 0:
        body = "No pages yet."
    else:
        body = "<ul>"
        for p in pages:
            body += html_item_template.format(name=p.name)
        body += "</ul>"
    return(HttpResponse(html_template_css.format(body=body)))

@csrf_exempt
def page(request, name):

    if request.method == 'PUT':
        try:
            p = Page.objects.get(name=name)
        except Page.DoesNotExist:
            p = Page(name=name)
        p.content = request.body.decode("utf-8")
        p.save()

    if request.method == 'GET' or request.method == 'PUT':
        try:
            p = Page.objects.get(name=name)
            content = p.content
            if name.endswith('.css'):
                content_type='text/css'
            else:
                content_type='text/html'
            response = HttpResponse(content,content_type=content_type)
        except Page.DoesNotExist:
            response = HttpResponseNotFound("Page " + name + " not found")
        return(response)

def style(request):

    color_list = ["blue", "gold", "red", "aqua", "green", "black", "yellow"]
    color1 = random.choice(color_list)
    color2 = random.choice(color_list)

    content = """
    body {
margin: 10px 20% 50px 70px;
font-family: sans-serif;
color: """ + color1 + """;
background: """ + color2 + """;
}"""
    content_type = 'text/css'
    response = HttpResponse(content, content_type=content_type)
    return (response)